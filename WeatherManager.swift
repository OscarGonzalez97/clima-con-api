//
//  WeatherManager.swift
//  Clima
//
//  Created by Oscar on 2022-11-01.
//  Copyright © 2022 App Brewery. All rights reserved.
//

import Foundation

struct WeatherManager {
    let weatherURL = "https://api.openweathermap.org/data/2.5/weather?appid=f44c7dd937367fc5137a5f3aa6a12e7c"
    
    func obtenerClima (nombreCiudad: String) {
        let urlString = "\(weatherURL)&q=\(nombreCiudad)"
        performRequest(urlString: urlString)
    }
    
    func performRequest(urlString: String){
        // creamos url
        // hacemos un if let porque la url puede crearse mal por alguna razon
        // entonces para no tener un URL? usamos if let
        if let url = URL(string: urlString){
            // creamos una URLSession
            let session = URLSession(configuration: .default)
            // damos una task a la session
            let task = session.dataTask(with: url, completionHandler: manejador(data:urlResponse:error:))
            // ejecutamos la task
            task.resume()
            
        }
    }
    
    func manejador(data: Data?, urlResponse: URLResponse?, error: Error?) {
        if error != nil {
            print(error!)
            return
        }
        
        if let datos = data {
//            print(datos)
            parseJSON(climaData: datos)
        }
    }
    
    func parseJSON(climaData: Data){
        let decoder = JSONDecoder()
        do{
            let decodedData = try decoder.decode(WeatherData.self, from: climaData)
            print(decodedData.main.temp)
            print(decodedData.name)
            print(decodedData.weather[0].description)
        } catch {
            print(error)
        }
        
        
    }
}
