//
//  ViewController.swift
//  Clima
//
//  Created by Angela Yu on 01/09/2019.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    var weatherManager = WeatherManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
    }

    /// el textField `textField` nos avisa que se apretó el botón enter.
    /// Y nos pregunta si tiene que retornar. Le damos la respueta retornando una expresión Booleana (true o false)
    @IBAction func search(_ sender: UIButton) {
        print("se apretó buscar!")
        self.searchTextField.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("se apretó enter!")
        self.searchTextField.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let ciudad = textField.text {
            weatherManager.obtenerClima(nombreCiudad: ciudad)
        }
        
        searchTextField.text = ""
    }
}

